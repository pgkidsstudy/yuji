
<div id="crmWebToEntityForm" style="width:600px;margin:auto;">
    <form action="https://crm.zoho.com/crm/WebToLeadForm" 
    name="WebToLeads2439436000000271127" 
    method="post" 
    onsubmit="javascript:document.charset=&quot;UTF-8&quot;; return checkMandatory()" 
    accept-charset="UTF-8" 
    id="WebToLeads2439436000000271127">
        <!-- Do not remove this code. -->
        <input type="text" style="display:none;" name="xnQsjsdp"   value="1413d3ece9334fdce0ccdf3e8eba2d2229914c8b73dd7a4713fdfcd8455e4017" />
        <input type="hidden" name="zc_gad" id="zc_gad" value="" />
        <input type="text" style="display:none;" name="xmIwtLD"    value="a2f0827ac177d06818f426762a2523fa68adbdb7d3064a59f84925105c11ae9a" /> 
        <input type="text" style="display:none;" name="actionType" value="TGVhZHM=" /> 
        <input type="text" style="display:none;" name="returnURL"  value="https://smakids.com/playcomplete/" />
        <!-- Do not remove this code. -->
        <h1 class="comform-title">
            スマートキッズイングリッシュ説明会申込フォーム
        </h1>

        <div class="comform-wrap">
            <div class="comform-container">
                <div class="comform-row">
                    <div class="itemTitle">
                        保護者様のお名前<span class="req-mark">必須</span>
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <input type="text" name="Company" id="tkna001" size="20" value="" style="width: 250px;" required="required" class="required" />
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        お名前<span class="req-mark">必須</span>
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <input type="text" name="Last Name" id="tkna001" size="20" value="" style="width: 250px;" required="required" class="required" />
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        学年<span class="req-mark">必須</span>
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <select class="itemChoice required" style="width:250px;" name="LEADCF7" required="required">
                            <!-- {{{ -->

                            <option value="-None-">
                                学年を選択
                            </option>
                            <option value="小学１年生">
                                小学１年生
                            </option>
                            <option value="小学２年生">
                                小学２年生
                            </option>
                            <option value="小学３年生">
                                小学３年生
                            </option>
                            <option value="小学４年生">
                                小学４年生
                            </option>
                            <option value="小学５年生">
                                小学５年生
                            </option>
                            <option value="小学６年生">
                                小学６年生
                            </option><!-- }}} -->
                        </select>
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        メール<span class="req-mark">必須</span>
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <input type="text" name="Email" id="tkna001" size="255" value="" style="width: 90%; min-width:250px;" required="required" class="required" />
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        電話番号<span class="req-mark">必須</span>
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <input type="text" name="Phone" id="tkna001" size="255" value="" style="width: 250px;" required="required" class="required" />
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        希望の連絡手段<span class="req-mark">必須</span>
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <select class="itemChoice required" style="width:250px;" name="LEADCF4">
                            <option value="メールが良い">
                                メールが良い
                            </option>
                            <option value="電話が良い">
                                電話が良い
                            </option>
                        </select>
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        電話希望場合の連絡時間帯
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <input type="text" name="LEADCF6" id="tkna001" size="255" value="" style="width: 250px;" />
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        希望教室<span class="req-mark">必須</span>
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <select class="itemChoice required" style="width:250px;" name="LEADCF10" id="areaselect">
                            <option value="池尻大橋校">
                                池尻大橋校
                            </option>
                            <option value="二子玉川校">
                                二子玉川校
                            </option>
                        </select>
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        説明会第1希望<span class="req-mark">必須</span>
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <select class="itemChoice required" style="width:170px;" id="day-1st" name="day-1st">
                            <!-- {{{ -->
                            %%ike_options%%
                           <!-- }}} -->
                        </select> <select style="width:170px;" id="hour-1st" name="hour-1st">
                            <!-- {{{ -->
                            <option value="-None-">
                                時間帯を選択
                            </option>
                            <option value="15時30分～">
                                15時30分～
                            </option>
                            <option value="17時00分～">
                                17時00分～
                            </option>
                            <option value="18時30分～">
                                18時30分～
                            </option>
                            <option value="20時00分～">
                                20時00分～
                            </option><!-- }}} -->
                        </select>
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        説明会第2希望
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <select class="itemChoice required" style="width:170px;" id="day-2nd" name="day-2nd">
                            <!-- {{{ -->
                            %%ike_options%%
                           <!-- }}} -->
                        </select> <select style="width:170px;" id="hour-2nd" name="hour-2nd">
                            <!-- {{{ -->
                            <option>
                                時間帯を選択
                            </option>
                            <option value="15時30分～">
                                15時30分～
                            </option>
                            <option value="17時00分～">
                                17時00分～
                            </option>
                            <option value="18時30分～">
                                18時30分～
                            </option>
                            <option value="20時00分～">
                                20時00分～
                           </option><!-- }}} -->
                        </select>
                    </div>
                    <!-- itemInput -->
                </div>

                <div class="comform-row">
                    <div class="itemTitle">
                        説明会第3希望
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <select class="itemChoice required" style="width:170px;" id="day-3rd" name="day-3rd">
                            <!-- {{{ -->
                            %%ike_options%%
                           <!-- }}} -->
                        </select> <select style="width:170px;" id="hour-3rd" name="hour-3rd">
                            <!-- {{{ -->

                            <option>
                                時間帯を選択
                            </option>
                            <option value="15時30分～">
                                15時30分～
                            </option>
                            <option value="17時00分～">
                                17時00分～
                            </option>
                            <option value="18時30分～">
                                18時30分～
                            </option>
                            <option value="20時00分～">
                                20時00分～
                            </option><!-- }}} -->
                        </select>
                    </div>
                    <!-- itemInput -->
                </div>
                <!-- 説明会第1希望 -->
                <input type="hidden" style="width:250px;" maxlength="255" name="LEADCF14" /> 
                <!-- 説明会第2希望 -->
                <input type="hidden" style="width:250px;" maxlength="255" name="LEADCF15" /> 
                <!-- 説明会第3希望 -->
                <input type="hidden" style="width:250px;" maxlength="255" name="LEADCF13" /> 
                <!-- サービス -->
                <input type="hidden" style="width:250px;" maxlength="255" name="LEADCF9" value="スマートキッズイングリッシュ" />
                <div class="comform-row">
                    <div class="itemTitle">
                        ご意見ご要望
                    </div>
                    <!-- itemTitle -->

                    <div class="itemInput">
                        <textarea name="LEADCF1" maxlength="2000" style="width:90%;">
</textarea>
                    </div>
                    <!-- itemInput -->
                </div>
            </div>

            <div id="form-button" class="form-buttons">
                <button type="submit" onkeydown="kc=event.keyCode;if(kc == 13 || kc == 32)goSubmit(this.form);" onmouseup="goSubmit(this.form);">申し込み</button>
            </div>
        </div>
        <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
        <script type="text/javascript">
//<![CDATA[
          
          var mndFileds =new Array("Company","Last Name","LEADCF7","Email" ,"Phone",   "day-1st","hour-1st");
          var fldLangVal=new Array("保護者名","お名前",  "学年",   "メール","電話番号","説明会日程","説明会時間");
                var name="";
                var email="";
          var areaElement = document.getElementById( "areaselect" ) ;
          areaElement.onchange = function() {
             var selectVal = $("#areaselect").val();
             if (selectVal == '池尻大橋校'){
	         $('#day-1st').children().remove();
	         $('#day-2nd').children().remove();
	         $('#day-3rd').children().remove();
             $('#day-1st').append('%%ike_options%%');
             $('#day-2nd').append('%%ike_options%%');
             $('#day-3rd').append('%%ike_options%%');
             }else{
	         $('#day-1st').children().remove();
	         $('#day-2nd').children().remove();
	         $('#day-3rd').children().remove();
             $('#day-1st').append('%%niko_options%%');
             $('#day-2nd').append('%%niko_options%%');
             $('#day-3rd').append('%%niko_options%%');
             }
          };
    function checkMandatory() {
             //第1希望
             var day1st = document.forms["WebToLeads2439436000000271127"]["day-1st"].value + " "+
                          document.forms["WebToLeads2439436000000271127"]["hour-1st"].value;
             document.forms["WebToLeads2439436000000271127"]["LEADCF14"].value = day1st;
             //第2希望
             var day1st = document.forms["WebToLeads2439436000000271127"]["day-2nd"].value + " "+
                          document.forms["WebToLeads2439436000000271127"]["hour-2nd"].value;
             document.forms["WebToLeads2439436000000271127"]["LEADCF15"].value = day1st;
             //第3希望
             var day1st = document.forms["WebToLeads2439436000000271127"]["day-3rd"].value + " "+
                          document.forms["WebToLeads2439436000000271127"]["hour-3rd"].value;
             document.forms["WebToLeads2439436000000271127"]["LEADCF13"].value = day1st;

              for(i=0;i<mndFileds.length;i++) {
                  var fieldObj=document.forms["WebToLeads2439436000000271127"][mndFileds[i]];
                  if(fieldObj) {
                        if (((fieldObj.value).replace(/^\s+|\s+$/g, "")).length==0) {
                         if(fieldObj.type =="file")
                                { 
                                 alert("アップロードするファイルを選択してください"); 
                                 fieldObj.focus(); 
                                 return false;
                                } 
                        alert(fldLangVal[i] +" を入力してください。"); 
                          fieldObj.focus();
                          return false;
                        }  else if(fieldObj.nodeName=="SELECT") {
                         if(fieldObj.options[fieldObj.selectedIndex].value=="-None-") {
                                alert(fldLangVal[i] +" を入力してください。"); 
                                fieldObj.focus();
                                return false;
                           }
                        } else if(fieldObj.type =="checkbox"){
                         if(fieldObj.checked == false){
                                alert("Please accept  "+fldLangVal[i]);
                                fieldObj.focus();
                                return false;
                           } 
                         } 
                         try {
                             if(fieldObj.name == "Last Name") {
                                name = fieldObj.value;
                            }
                        } catch (e) {}
                    }
                }
             }
           
        //]]>
        </script>
    </form>
</div>
