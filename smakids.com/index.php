<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta http-equiv="x-ua-compatible" content="IE=edge">
<title>smart kids english</title>
</head>
<body>
<header>
<div class="header-contents">
<h1>スマートキッズイングリッシュ：説明会募集日程フォーム作成フォーム</h1>
</div><!-- /.header-contents -->
</header>
<div class="main-wrapper">
<section>
<?php
require_once('days.php');
require_once('output.php');
?>
<form action="./" method="POST" name="">
    <div>
    <label>池尻大橋校</label>
    <textarea cols=80 rows=10 name="ike_day"><?= $current1; ?></textarea>
    </div>
    <div>
    <label>二子玉川校</label>
    <textarea cols=80 rows=10 name="niko_day"><?= $current2;?></textarea>
    </div>
    <button type="submit" value="登録">登録</button>
</form>
以下を実際のサイトに貼り付けて下さい
<textarea cols=80 rows=10>
<?php echo htmlspecialchars($output); ?>
</textarea>
</section>
</div><!-- /.main-wrapper -->
</body>
</html>

