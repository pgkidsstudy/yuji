<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <title>HTML/CSS/JavaScript学習</title>
    </head>
    <body>
        <div id="contents">
            <h1>学習成果一覧</h1>
            <div>
<?php

$type= [  //{{{
    '01_HTML'            => 13,
    '02_CSS'             => 24,
    '03_JavaScript'      => 23,
    '04_JavaScriptCanvas'=> 2,
    '05_Game1' =>3,
    '06_Game2' =>3,
    '07_Game3' =>3,
    '08_Phina' =>3,
    '09_Block' =>3,
];//}}}

$jstext_menu =[ //{{{
'2-01_console',
'2-02_tag',
'2-03_alert',
'2-04_html',
'3-01_if',
'3-02_var',
'3-03_elseif',
'3-04_comparison',
'3-05_logical',
'3-06_for',
'3-07_while',
'3-08_function',
'3-09_fizzbuzz',
'3-10_array',
'3-11_object',
'4-01_input',
'4-02_12hour',
'4-03_digit',
'4-04_math',
'5-01_countdown',
'5-02_location',
'5-03_cookie',
'5-04_image',
'5-05_slide',
'6-01_menu',
'6-02_box',
'6-03_ajax',
'7-01_rss',
'7-02_photo',
];//}}}

foreach ($type as $key=>$repeat){
    if ($key !=='03_JavaScript'){
       view($key, $repeat);
    }else{
        jsTextView($key,$jstext_menu);
    }
}

function view($type, $repeat){
    echo '<h2>'.$type.'</h2>';
    for($i=1;$i<=$repeat;$i++){
        $file     = 'ct1/'.$type.'/work'.sprintf("%02d", $i).'.html';
        $linkname = 'work'.sprintf("%02d", $i).'.html';
        if (file_exists(dirname(__FILE__).'/'.$file)){
            echo '[<a href='.$file .'>'.$linkname.'</a>] ';
        }else{
            echo '['.$linkname.'] ';
        }
        echo " ";
    }
    echo '<hr>';
}
function jsTextView($type,$jstext_menu)
{
    echo '<h2>'.$type.'</h2>';
    foreach($jstext_menu as $val){
        $file     = 'ct1/'.$type.'/practice/'.$val . '/index.html';
        $linkname = $val;
        if (file_exists(dirname(__FILE__).'/'.$file)){
            echo '[<a href='.$file .'>'.$linkname.'</a>] ';
        }else{
            echo '['. $linkname.']';
        }
    }
    echo '<hr />';
}
